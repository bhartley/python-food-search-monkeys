# -*- coding: utf-8 -*-

import os, sys
import time, random
import logging

from Grid import Grid
from tick import tick

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('x',
                    help='width of area',
                    type=int)
parser.add_argument('y',
                    help='height of area',
                    type=int)
parser.add_argument('monkeys',
                    help='number of monkeys to set loose',
                    type=int)
args = parser.parse_args()
monkeys, x_test, y_test = args.monkeys, args.x, args.y

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

time_interval = 1 / 4

def Main():
    ## Check that number of monkeys is non-zero
    living_monkeys = False
    if monkeys > 0:
        living_monkeys = True
    else:
        print('Number of monkeys must be greater than 0')
        sys.exit()

    ## Make new grid
    grid = Grid(x_test, y_test)

    ## Load the monkeys
    for x in range(0, monkeys):
        monkey_x = random.choice(range(0, x_test - 1))
        monkey_y = random.choice(range(0, y_test - 1))
        grid.add_monkey(monkey_x, monkey_y, x)
    time.sleep(1)

    ## Run wild
    while living_monkeys:
        x = grid.total_living_monkeys()
        if x == 1:
            the_monkeys = grid.get_monkeys()
            for index, monkey in enumerate(the_monkeys):
                if monkey.dead == False:
                    os.system('clear')
                    print('Only one living monkey remains, monkey ' + str(monkey.index) + ' is the winner!')
                    living_monkeys = False
                    grid.show_grid()
        elif x > 1:
            tick(grid)
            time.sleep(time_interval)
            y = grid.total_living_monkeys()
            if y == 0: # multiple monkeys were killed this round (tie)
                msg = 'no remaining monkeys, monkeys '
                for index, monkey_index in enumerate(grid.died_this_round):
                    msg += str(monkey_index)
                    if index < len(grid.died_this_round) - 1:
                        msg += ', '
                msg += ' tie for first'
                print(msg)
                living_monkeys = False
            elif y > 1: # Will have to do another round so clear the screen
                os.system('clear')
        else:
            print('no more monkeys')
            living_monkeys = False
    return

if __name__ == '__main__':
    Main()
