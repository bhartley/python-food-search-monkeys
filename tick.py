# -*- coding: utf-8 -*-

import logging
from threading import Thread

def tick(grid):
    grid.clear_died_this_round()
    grid.show_grid()
    threads = []
    for index, monkey in enumerate(grid.get_monkeys()):
        if monkey.dead == True:
            logging.debug('monkey ' + str(monkey.index) + ' is dead')
            continue
        t = Thread(target=take_turn, args=(monkey,))
        threads.append(t)
        t.start()
    return

def take_turn(monkey):
    monkey.take_turn()
    return