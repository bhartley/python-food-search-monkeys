# -*- coding: utf-8 -*-

import uuid, random, time
import logging

import moves_config as moves
from resources_config import starting_resources
from resources_config import thresholds

class Monkey:
    def __init__ (self, grid, x, y, index):
        self.x_pos = x
        self.y_pos = y
        self.index = index
        self.grid = grid
        self.id = uuid.uuid4()
        self.dead = False
        self.energy = starting_resources['e']

    def position (self):
        return {
                'x': self.x_pos,
                'y': self.y_pos
                }

    def get_id (self):
        return self.id

    def get_available_moves (self):
        available_moves = []
        pos = self.position()
        x, y = pos['x'], pos['y']
        max_x, max_y = self.grid.x_dim - 1, self.grid.y_dim - 1

        if y > 0:
            available_moves.append(moves.UP)        ## 0
        if x < max_x:
            available_moves.append(moves.RIGHT)     ## 1
        if y < max_y:
            available_moves.append(moves.DOWN)      ## 2
        if x > 0:
            available_moves.append(moves.LEFT)      ## 3
        if y > 0 and x < max_x:
            available_moves.append(moves.UP_RIGHT)  ## 4
        if y > 0 and x > 0:
            available_moves.append(moves.UP_LEFT)   ## 5
        if y < max_y and x < max_x:
            available_moves.append(moves.DOWN_RIGHT)## 6
        if y < max_y and x > 0:
            available_moves.append(moves.DOWN_LEFT) ## 7
        return available_moves

    def do_look (self):
        spot = self.grid.look(self.x_pos, self.y_pos)
        return { 'f': spot['f'], 'g': spot['g'] }

    def do_move (self, direction):
        if direction in self.get_available_moves():
            logging.debug('monkey ' + str(self.index) + ' moving')
            if direction == moves.UP:
                self.y_pos -= 1
#                print('moving UP')
            if direction == moves.RIGHT:
                self.x_pos += 1
#                print('moving RIGHT')
            if direction == moves.DOWN:
                self.y_pos += 1
#                print('moving DOWN')
            if direction == moves.LEFT:
                self.x_pos -= 1
#                print('moving LEFT')
            if direction == moves.UP_RIGHT:
                self.y_pos -= 1
                self.x_pos += 1
#                print('moving UP_RIGHT')
            if direction == moves.UP_LEFT:
                self.y_pos -= 1
                self.x_pos -= 1
#                print('moving UP_LEFT')
            if direction == moves.DOWN_LEFT:
                self.y_pos += 1
                self.x_pos -= 1
#                print('moving DOWN_LEFT')
            if direction == moves.DOWN_RIGHT:
                self.y_pos += 1
                self.x_pos += 1
#                print('moving DOWN_RIGHT')
            self.use_energy(1)
        return

    def do_move_random (self):
        dirs = self.get_available_moves()
        go = random.choice(dirs)
        self.do_move(go)
        return

    def do_eat (self):
        food = self.do_look()['f']
        pos = self.position()
        if (food > 0):
            self.grid.remove_food(pos['x'], pos['y'], 1)
            logging.debug('monkey ' + str(self.index) + ' eating')
        self.energy += 1
        return

    def do_die (self):
        print('monkey ' + str(self.index) + ' died!')
        self.grid.remove_monkey(self.id)
        self.grid.died_this_round.append(self.index)
        return

    def take_turn (self):
        resources_here = self.do_look()
        food = resources_here['f']

        if food < thresholds['f']:
            self.do_move_random()
        else:
            self.do_eat()
        return

    def use_energy (self, e):
        self.energy -= e
        if self.energy <= 0:
            self.do_die()
        return