#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 13:45:40 2017

@author: Benjamin Hartley

"""

from Monkey import Monkey

from resources_config import starting_resources

class Grid:
    def __init__ (self, x, y):
        self.x_dim = x
        self.y_dim = y
        self.grid = self.newGrid(x, y)
        self.monkeys = []
        self.died_this_round = []

    def newGrid (self, x, y):
        grid = []
        for m in range(0, y):
            tmp = []
            for n in range(0, x):
                spot = {
                        'f': starting_resources['f'],  # food
                        'g': starting_resources['g']   # gold
                        }
                tmp.append(spot)
            grid.append(tmp)
        return grid

    def show_grid (self):
        positions = self.get_monkey_positions()

        for m in range(0, self.y_dim):
            msg = '['
            for n in range(0, self.x_dim):
                monkeys_here = 0
                indices = [i for i, x in enumerate(positions) if x == (n,m)]
                monkeys_here += len(indices)
                if monkeys_here > 3:
                    msg += ' X' + str(self.grid[m][n]['f']) + 'X '
                elif monkeys_here > 2:
                    msg += ' X' + str(self.grid[m][n]['f']) + '* '
                elif monkeys_here > 1:
                    msg += ' *' + str(self.grid[m][n]['f']) + '* '
                elif monkeys_here > 0:
                    msg += ' *' + str(self.grid[m][n]['f']) + '  '
                else:
                    msg += '  ' + str(self.grid[m][n]['f']) + '  '
#                    if n == self.x_dim - 1:
#                        msg += '|'
            msg += ']\n'
            print(msg)
        return

    def get_monkey_positions (self):
        positions = []
        for index, monkey in enumerate(self.monkeys):
            if monkey.dead == False:
                pos = monkey.position()
                positions.append((pos['x'], pos['y']))
        return positions

    def look (self, x, y):
        return { 'f': self.grid[y][x]['f'], 'g': self.grid[y][x]['g'] }

    def add_monkey (self, x, y, index):
        if x >= self.x_dim or x < 0:
            print('out of range, not adding')
            return
        elif y >= self.y_dim or y < 0:
            print('out of range, not adding')
            return

        tmp = Monkey(self, x, y, index)
        self.monkeys.append(tmp)
        print('monkey added')
        return

    def remove_monkey (self, id):
        for monkey in self.monkeys:
            if monkey.id == id:
                monkey.dead = True
                break
        return

    def get_monkeys (self):
        return self.monkeys

    def show_monkeys (self):
        print('total living monkeys:', self.total_living_monkeys())
        if self.total_living_monkeys() > 0:
            print('monkey positions:')
            for index, monkey in enumerate(self.monkeys):
                if monkey.dead == True:
                    print('monkey ' + str(monkey.id) + ' is dead')
                else:
                    pos = monkey.position()
                    print(str(index + 1) + ':  (' + str(pos['x']) + ',' + str(pos['y']) + ')')
            return True
        else:
            print('no monkeys to show')
            return False

    def total_living_monkeys (self):
        l = len(self.monkeys)
        for x in range(0,l):
            if self.monkeys[x].dead == True:
                l -= 1
        return l

    def add_food (self, x, y, amt):
        return

    def remove_food (self, x, y, amt):
        food = self.grid[y][x]['f']
        if food > amt:
            food -= amt
        else:
            food = 0
        self.grid[y][x]['f'] = food
        return

    def clear_died_this_round (self):
        self.died_this_round = []
        return
